<div id="footer" class="color-div">
    <script type="text/javascript">
        (function(d,s,id){var z=d.createElement(s);z.type="text/javascript";z.id=id;z.async=true;z.src="//static.zotabox.com/d/8/d89fb030f7d5b6e15356b5fd9f888c4a/widgets.js";var sz=d.getElementsByTagName(s)[0];sz.parentNode.insertBefore(z,sz)}(document,"script","zb-embed-code"));
    </script>
    <div class="container">
        <div class="row">
            <div class="col-sm-4">
                <div class="widget">
                    <h4 class="widget-title"><a href="{{route('trang-chu')}}" id="logo"><img src="assets/dest/images/logo1.png" width="200px" alt=""></a></h4>
                   
                </div>
            </div>
            <div class="col-sm-4">
                <div class="widget">
                    <h4 class="widget-title">Thông tin</h4>
                    <div>
                        <ul>
                            <li><a href="blog_fullwidth_2col.html"><i class="fa fa-chevron-right"></i>	Địa chỉ: Tổ 28, P.Phan Đình Phùng, TP.Thái Nguyên
</a></li>
                            <li><a href="blog_fullwidth_2col.html"><i class="fa fa-chevron-right"></i> 	Website: http://ldtrungviet.com</a></li>
                            <!-- <li><a href="blog_fullwidth_2col.html"><i class="fa fa-chevron-right"></i> Marketing</a></li>
                            <li><a href="blog_fullwidth_2col.html"><i class="fa fa-chevron-right"></i> Tips</a></li>
                            <li><a href="blog_fullwidth_2col.html"><i class="fa fa-chevron-right"></i> Resources</a></li>
                            <li><a href="blog_fullwidth_2col.html"><i class="fa fa-chevron-right"></i> Illustrations</a></li> -->
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="col-sm-10">
                    <div class="widget">
                        <h4 class="widget-title">Liên hệ</h4>
                        <div>
                            <div class="contact-info">
                                <i class="fa fa-map-marker"></i>
                                <p>Sđt:Điện thoại: 0868 603 832
</p>
                                <p>Email: huachicuong90@gmail.com
</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div> <!-- .row -->
    </div> <!-- .container -->
</div> <!-- #footer -->
<div class="copyright">
    <div class="container">
        <p class="pull-left">Trang Huyen(&copy;) 2018</p>
      
        <div class="clearfix"></div>
    </div> <!-- .container -->
</div> <!-- .copyright -->