<div id="header">
    <div class="header-top">
        <div class="container">
            <div class="pull-left auto-width-left">
                <ul class="top-menu menu-beta l-inline">
                    <li><a href="#"><i class="fa fa-home"></i>Đ/C:Tổ 28,Đ.Hoàng Ngân, TP.Thái Nguyên</a></li>
                    <li><a href="#"><i class="fa fa-phone"></i>Tell:0868 603 832</a></li>
                </ul>
            </div>
            <div class="pull-right auto-width-right">
                <ul class="top-details menu-beta l-inline">
                    @if(Auth::check())
                        <li><a href="{{route('profile',Auth::user()->id)}}"><i class="fa fa-user"></i>{{ Auth::user()->name}}</a></li>
                        <li><a href="{{route('dangxuat')}}">Đăng xuất</a></li>
                    @else
                    <li><a href="{{route('dangky')}}">Đăng kí</a></li>
                    <li><a href="{{route('dangnhap')}}">Đăng nhập</a></li>
                    @endif
                </ul>
            </div>
            <div class="clearfix"></div>
        </div> <!-- .container -->
    </div> <!-- .header-top -->
    <div class="header-body">
        <div class="container beta-relative">
            <div class="pull-left">
                <a href="{{route('trang-chu')}}" id="logo"><img src="assets/dest/images/logo1.png" width="200px" alt=""></a>
            </div>
            <div class="pull-right beta-components space-left ov">
                <div class="space10">&nbsp;</div>
                <div class="beta-comp">
                    <form role="search" method="get" id="searchform" action="{{route('Search-Product')}}">
                        {{--{{csrf_field()}}--}}
                        <input type="text" value="" name="txtsearch"  placeholder="Nhập từ khóa..." />
                        <button class="fa fa-search" type="submit" id="searchsubmit"></button>
                    </form>
                </div>

                <div class="beta-comp">
                    @if(Session::has('cart'))
                    <div class="cart">
                        <div class="beta-select"><i class="fa fa-shopping-cart"></i> Giỏ hàng (
                            @if(Session::has('cart')) {{Session('cart')->totalQty.")"}}
                            @else trống
                            @endif<i class="fa fa-chevron-down"></i></div>

                        <div class="beta-dropdown cart-body">
                            @foreach($product_cart as $product)
                            <div class="cart-item">
                                <a class="cart-item-delete" href="{{route('xoagiohang',$product['item']['id'])}}"><i class="fa fa-times"></i></a>
                                <a class="cart-item-minus" href="{{route('xoaonegiohang',$product['item']['id'])}}"><i class="fa fa-minus"></i></a>
                                <a class="cart-item-plus" href="{{route('themgiohang',$product['item']['id'])}}"><i class="fa fa-plus"></i></a>
                                <div class="media">
                                    <a class="pull-left" href="{{route('chitietsanpham',[$product['item']['id'],$product['item']['alias']])}}"><img src="uploads/product/{{$product['item']['image']}}" alt=""></a>
                                    <div class="media-body">
                                        <span class="cart-item-title">{{$product['item']['name']}}</span>
                                        {{--<span class="cart-item-options">Size: XS; Colar: Navy</span>--}}
                                        <span class="cart-item-amount">{{$product['qty'].':'}}<span>
                                                @if($product['qty'] *$product['item']['promotion_price']==0)
                                                    {{$product['qty'] *$product['item']['unit_price']}},000vnđ
                                                    @else
                                                    {{$product['qty'] *$product['item']['promotion_price']}},000vnđ
                                                @endif
                                            </span></span>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                            <div class="cart-caption">
                                <div class="cart-total text-right">Tổng tiền: <span class="cart-total-value">{{Session('cart')->totalPrice}},000vnđ</span></div>
                                <div class="clearfix"></div>

                                <div class="center">
                                    <div class="space10">&nbsp;</div>
                                    <a href="{{route('checkout')}}" class="beta-btn primary text-center">Đặt hàng <i class="fa fa-chevron-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div> <!-- .cart -->
                        @endif 
                </div>
            </div>
            <div class="clearfix"></div>
        </div> <!-- .container -->
    </div> <!-- .header-body -->
    <div class="header-bottom" style="background-color: #0277b8;">
        <div class="container">
            <a class="visible-xs beta-menu-toggle pull-right" href="#"><span class='beta-menu-toggle-text'>Menu</span> <i class="fa fa-bars"></i></a>
            <div class="visible-xs clearfix"></div>
            <nav class="main-menu">
                <ul class="l-inline ov">

                    <li><a href="{{route('trang-chu')}}">Home</a></li>

                    <li><a href="#">Sản phẩm</a>
                        <ul class="sub-menu">
                            @foreach($loaisp as $item)
                            <li><a href="{{route('loaisanpham',[$item->id,$item->alias])}}">{{$item->name}}</a></li>
                            @endforeach
                        </ul>
                    </li>
                    <li><a href="{{route('gioi-thieu')}}">Giới thiệu</a></li>
                    <li><a href="{{route('lien-he')}}">Liên hệ</a></li>
                    <li><a href="{{route('quan-tam')}}">WishList</a></li>
                </ul>
                <div class="clearfix"></div>
            </nav>
        </div> <!-- .container -->
    </div> <!-- .header-bottom -->
</div> <!-- #header -->