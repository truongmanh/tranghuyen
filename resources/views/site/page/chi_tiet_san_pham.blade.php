@extends('site.master')
@section('title','Chi tiết sản phẩm')
@section('content')
<div class="inner-header">
    <div class="container">
        <div class="pull-left">
            <h6 class="inner-title">Chi tiết sản phẩm</h6>
        </div>
        <div class="pull-right">
            <div class="beta-breadcrumb font-large">
                <a href="{{route('trang-chu')}}">Trang chủ</a> / <span>Chi tiết sản phẩm</span>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>

<div class="container">
    <div id="content">
        <div class="row">
            <div class="col-sm-9">

                <div class="row">

                    <div class="col-sm-4">
                        @if($detail->promotion_price!=0)
                            <div class="ribbon-wrapper"><div class="ribbon sale">Sale</div></div>
                        @endif
                        <img src="uploads/product/{{$detail->image}}" alt="">
                    </div>
                    <div class="col-sm-8">
                        <div class="single-item-body">
                            <p class="single-item-title">{{$detail->name}}</p>
                            <p class="single-item-price">
                                @if($detail->promotion_price==0)
                                    <span>{{number_format($detail->unit_price,3,',','.')}}vnđ</span>
                                @else
                                    <span class="flash-del">{{number_format($detail->unit_price,3,',','.')}}vnđ</span>
                                    <span class="flash-sale">{{number_format($detail->promotion_price,3,',','.')}}vnđ</span>
                                @endif
                            </p>
                        </div>

                        <div class="clearfix"></div>
                        <div class="space20">&nbsp;</div>

                        <!-- <div class="single-item-desc">
                            <p>{{$detail->description}}</p>
                        </div> -->
                        <div class="space20">&nbsp;</div>

                        <p>Đặt hàng:<span style="color: red">{{$detail->buy_count}}</span></p>
                        <div class="single-item-options">
                            <a class="add-to-cart" href="{{route('themgiohang',$detail->id)}}"><i class="fa fa-shopping-cart"></i></a>
                            <div class="clearfix"></div>
                        </div>

                        <p id="view_count{{$detail->id}}">Quan tâm:<span style="color: red" >{{$detail->view_count}}</span></p>
                        <div class="single-item-options" id="like-btn">
                            @if($data_w=="no")
                           <button class="add-wishlist like-btn" value="{{$detail->id}}"><i class="fa fa-heart"></i></button>
                                @else
                                <button class="btn btn-primary btn-huy" value="{{$detail->id}}">Hủy quan tâm</button>
                            @endif
                                <div class="clearfix"></div>
                        </div>
                        <p>Đánh giá:</p>
                        <div>
                         <!-- <img src="assets/dest/images/sao.png" width="30px" alt=""> -->
                         @foreach($detail_star as $item)
                            @if($item->view_count>=1 && $item->view_count<=50)
                            <img src="assets/dest/images/sao.png" width="30px" alt="">
                            @elseif($item->view_count<=200 && $item->view_count>=51)
                            <img src="assets/dest/images/sao.png" width="30px" alt="">
                            <img src="assets/dest/images/sao.png" width="30px" alt="">
                            @elseif($item->view_count<=500 && $item->view_count>=201)
                            <img src="assets/dest/images/sao.png" width="30px" alt="">
                            <img src="assets/dest/images/sao.png" width="30px" alt="">
                            <img src="assets/dest/images/sao.png" width="30px" alt="">
                            @elseif($item->view_count<=1000 && $item->view_count>=501)
                            <img src="assets/dest/images/sao.png" width="30px" alt="">
                            <img src="assets/dest/images/sao.png" width="30px" alt="">
                            <img src="assets/dest/images/sao.png" width="30px" alt="">
                            <img src="assets/dest/images/sao.png" width="30px" alt="">
                            @elseif($item->view_count>=1001)
                            <img src="assets/dest/images/sao.png" width="30px" alt="">
                            <img src="assets/dest/images/sao.png" width="30px" alt="">
                            <img src="assets/dest/images/sao.png" width="30px" alt="">
                            <img src="assets/dest/images/sao.png" width="30px" alt="">
                            <img src="assets/dest/images/sao.png" width="30px" alt="">
                                @else
                                {!!"<br />" !!}
                            @endif

                         @endforeach()
                        </div>
                    </div>
                </div>

                <div class="space40">&nbsp;</div>
                <div class="woocommerce-tabs">
                    <ul class="tabs">
                        <li><a href="#tab-description">Chi tiết</a></li>
                        <li><a href="#tab-reviews">Bình luận</a></li>
                    </ul>

                    <div class="panel" id="tab-description">
                        <p>{!!$detail->description!!}</p>
                    </div>
                    <div class="panel" id="tab-reviews">
                        <div class="fb-comments" data-href="<?php echo $_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI']; ?>" data-width="760px" data-numposts="5"></div>
                    </div>
                </div>
                <div class="space50">&nbsp;</div>
                <div class="beta-products-list">
                    <h4>Sản phẩm tương tự</h4>
                    <div class="space60">&nbsp;</div>
                    <div class="row">
                        @foreach($spsame as $detail)
                        <div class="col-sm-4">
                            <div class="single-item">
                                @if($detail->promotion_price!=0)
                                    <div class="ribbon-wrapper"><div class="ribbon sale">Sale</div></div>
                                @endif
                                <div class="single-item-header">
                                    <a href="{{route('chitietsanpham',[$detail->id,$detail->alias])}}"><img src="uploads/product/{{$detail->image}}" height="250px" alt=""></a>
                                </div>
                                <div class="single-item-body">
                                    <p class="single-item-title">{{$detail->name}}</p>
                                    <p class="single-item-price">
                                        @if($detail->promotion_price==0)
                                            <span>{{number_format($detail->unit_price,3,',','.')}}vnđ</span>
                                        @else
                                            <span class="flash-del">{{number_format($detail->unit_price,3,',','.')}}vnđ</span>
                                            <span class="flash-sale">{{number_format($detail->promotion_price,3,',','.')}}vnđ</span>
                                        @endif
                                    </p>
                                </div>
                                <div class="single-item-caption">
                                    <a class="add-to-cart pull-left" href="{{route('themgiohang',$detail->id)}}"><i class="fa fa-shopping-cart"></i></a>
                                    <a class="beta-btn primary" href="{{route('chitietsanpham',[$detail->id,$detail->alias])}}">Chi tiết <i class="fa fa-chevron-right"></i></a>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                            @endforeach
                    </div>
                </div> <!-- .beta-products-list -->

            </div>
            <div class="col-sm-3 aside">
                <div class="widget">
                    <h3 class="widget-title">Sản phẩm SALE</h3>
                    <div class="widget-body">
                        <div class="beta-sales beta-lists">
                        @foreach($sale_product as $item)
                            <div class="media beta-sales-item">
                                <a class="pull-left" href="{{route('chitietsanpham',[$item->id,$item->alias])}}"><img src="uploads/product/{{$item->image}}" alt="" width="100px"></a>
                                <div class="media-body">
                                  {{$item->name}}
                                  <p>
                                  @if($detail->promotion_price==0)
                                            <span>{{number_format($detail->unit_price,3,',','.')}}vnđ</span>
                                        @else
                                            <span class="flash-del">{{number_format($detail->unit_price,3,',','.')}}vnđ</span>
                                            <span class="flash-sale">{{number_format($detail->promotion_price,3,',','.')}}vnđ</span>
                                        @endif</p>
                                    <span class="beta-sales-price">
                                        
                                    </span>
                                </div>
                            </div>
                        @endforeach()
                        </div>
                    </div>
                </div> <!-- best sellers widget -->

            </div>
        </div>
    </div> <!-- #content -->
</div> <!-- .container -->
    @endsection