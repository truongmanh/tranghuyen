@extends('site.master')
@section('content')

    <div class="inner-header">
        <div class="container">
            <div class="pull-left">
                <h6 class="inner-title">Đăng nhập</h6>
            </div>
            <div class="pull-right">
                <div class="beta-breadcrumb">
                    <a href="{{route('trang-chu')}}">Home</a> / <span>Đăng nhập</span>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
        @if(Session::has('thong bao'))
            <div class="errors-login">
                <h6>{{Session::get('thong bao')}}</h6>
            </div>
        @endif
    <div class="container">
        <div id="content">
            <form action="{{route('dangnhap')}}" method="post" class="beta-form-checkout" id="formdn">
                {{csrf_field()}}
                <div class="row">
                    <div class="col-sm-2"></div>
                    <div class="col-sm-6">
                        <h4>Đăng nhập</h4>
                        <div class="space20">&nbsp;</div>
                        <div class="form-block">
                            <label for="name">Name*</label>
                            <input type="text" id="name" name="name" required>
                        </div>
                        <div class="form-block">
                            <label for="pass">Password*</label>
                            <input type="password" id="pass" name="pass" required>
                        </div>
                        <div class="form-block">
                            <button type="submit" class="btn btn-primary">Đăng nhập</button>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <h4>Hoặc Đăng nhập facebook</h4>
                        <a href="{{route('facebook/redirect')}}">Login with facebook</a>
                    </div>
                </div>

            </form>
        </div> <!-- #content -->
    </div> <!-- .container -->
@endsection()