@if(Session::has('mailmessage'))
    <div class="alert alert-danger">
        {{Session::get('mailmessage')}}
    </div>
@endif
@extends('site.master')
@section('title','trang chủ')
@section('content')

<div class="rev-slider">
    <div class="fullwidthbanner-container">
        <div class="fullwidthbanner">
            <div class="bannercontainer" >
                <div class="banner" >
                    <ul>
                        <!-- THE FIRST SLIDE -->
                        @foreach($silde as $value)
                        <li data-transition="boxfade" data-slotamount="20" class="active-revslide" style="width: 100%; height: 100%; overflow: hidden; z-index: 18; visibility: hidden; opacity: 0;">
                            <div class="slotholder" style="width:100%;height:100%;" data-duration="undefined" data-zoomstart="undefined" data-zoomend="undefined" data-rotationstart="undefined" data-rotationend="undefined" data-ease="undefined" data-bgpositionend="undefined" data-bgposition="undefined" data-kenburns="undefined" data-easeme="undefined" data-bgfit="undefined" data-bgfitend="undefined" data-owidth="undefined" data-oheight="undefined">
                                <div class="tp-bgimg defaultimg" data-lazyload="undefined" data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat" data-lazydone="undefined" src="uploads/slider/{{$value->link }}" data-src="uploads/slider/{{$value->link }}" style="background-color: rgba(0, 0, 0, 0); background-repeat: no-repeat; background-image: url('uploads/slider/{{$value->link }}'); background-size: cover; background-position: center center; width: 100%; height: 100%; opacity: 1; visibility: inherit;">
                                </div>
                            </div>

                        </li>
                        @endforeach

                    </ul>
                </div>
            </div>

            <div class="tp-bannertimer"></div>
        </div>
    </div>
    <!--slider-->
</div>
<div class="container">
        <div id="content" class="space-top-none">
            <div class="main-content">
                <div class="space60">&nbsp;</div> 
                <!-- SẢN PHẨM MỚI -->
                        <div class="beta-products-list">
                            <h4 style="color:red">Sản phẩm MỚI</h4>
                            <div class="row">
                                @foreach($new_product as $item)
                                <div class="col-sm-3">
                                    <div class="single-item">
                                        @if($item->promotion_price!=0)
                                            <div class="ribbon-wrapper"><div class="ribbon sale">Sale</div></div>
                                        @endif
                                        <div class="single-item-header">
                                            <a href="{{route('chitietsanpham',[$item->id,$item->alias])}}"><img src="uploads/product/{{$item->image}}" height="250px" alt=""></a>
                                        </div>
                                        <div class="single-item-body">
                                            <p class="single-item-title">{{$item->name}}</p>
                                            <p class="single-item-price">
                                            @if($item->promotion_price==0)
                                            <span>{{$item->unit_price}},000vnđ</span>
                                        @else
                                            <span class="flash-del">{{$item->unit_price}},000vnđ</span>
                                            <span class="flash-sale">{{$item->promotion_price}},000vnđ</span>
                                        @endif
                                            </p> 
                                        </div>
                                        <div class="single-item-caption">
                                        <a class="add-to-cart pull-left" href="{{route('themgiohang',$item->id)}}"><i class="fa fa-shopping-cart"></i></a>
                                        <a class="beta-btn primary" href="{{route('chitietsanpham',[$item->id,$item->alias])}}">Chi tiết <i class="fa fa-chevron-right"></i></a>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>
                                    @endforeach()
                                   
                            </div>
                            <div class="row">
                            <div class="pull-right">{{$new_product->links()}}</div></div>
                        </div> <!-- .beta-products-list -->
                <!-- SẢN PHẨM SALE -->
                        <div class="beta-products-list">
                            <h4 style="color:red">Sản phẩm SALE</h4>
                            <div class="row">
                                @foreach($sale_product as $item)
                                <div class="col-sm-3">
                                    <div class="single-item">
                                        @if($item->promotion_price!=0)
                                            <div class="ribbon-wrapper"><div class="ribbon sale">Sale</div></div>
                                        @endif
                                        <div class="single-item-header">
                                            <a href="{{route('chitietsanpham',[$item->id,$item->alias])}}"><img src="uploads/product/{{$item->image}}" height="250px" alt=""></a>
                                        </div>
                                        <div class="single-item-body">
                                            <p class="single-item-title">{{$item->name}}</p>
                                            <p class="single-item-price">
                                            @if($item->promotion_price==0)
                                            <span>{{$item->unit_price}},000vnđ</span>
                                        @else
                                            <span class="flash-del">{{$item->unit_price}},000vnđ</span>
                                            <span class="flash-sale">{{$item->promotion_price}},000vnđ</span>
                                        @endif
                                            </p>
                                        </div>
                                        <div class="single-item-caption">
                                        <a class="add-to-cart pull-left" href="{{route('themgiohang',$item->id)}}"><i class="fa fa-shopping-cart"></i></a>
                                        <a class="beta-btn primary" href="{{route('chitietsanpham',[$item->id,$item->alias])}}">Chi tiết <i class="fa fa-chevron-right"></i></a>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>
                                    @endforeach()
                            </div>
                            <div class="row">
                            <div class="pull-right">{{$sale_product->links()}}</div></div>
                        </div> <!-- .beta-products-list -->
                 <!-- SẢN PHẨM HOT -->
                 <div class="beta-products-list">
                            <h4 style="color:red">Sản phẩm HOT</h4>
                            <div class="row">
                                @foreach($hot_product as $item)
                                <div class="col-sm-3">
                                    <div class="single-item">
                                        @if($item->promotion_price!=0)
                                            <div class="ribbon-wrapper"><div class="ribbon sale">Sale</div></div>
                                        @endif
                                        <div class="single-item-header">
                                            <a href="{{route('chitietsanpham',[$item->id,$item->alias])}}"><img src="uploads/product/{{$item->image}}" height="250px" alt=""></a>
                                        </div>
                                        <div class="single-item-body">
                                            <p class="single-item-title">{{$item->name}}</p>
                                            <p class="single-item-price">
                                            @if($item->promotion_price==0)
                                            <span>{{$item->unit_price}},000vnđ</span>
                                        @else
                                            <span class="flash-del">{{$item->unit_price}},000vnđ</span>
                                            <span class="flash-sale">{{$item->promotion_price}},000vnđ</span>
                                        @endif
                                            </p>
                                        </div>
                                        <div class="single-item-caption">
                                            <a class="add-to-cart pull-left" href="{{route('themgiohang',$item->id)}}"><i class="fa fa-shopping-cart"></i></a>
                                            <a class="beta-btn primary" href="{{route('chitietsanpham',[$item->id,$item->alias])}}">Chi tiết <i class="fa fa-chevron-right"></i></a>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>
                                    @endforeach()
                                 
                            </div>
                           
                        </div> <!-- .beta-products-list -->
            </div> <!-- .main-content -->
        </div> <!-- #content -->
    </div>

@endsection