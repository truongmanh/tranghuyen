@extends('site.master')
@section('title','Loại sản phẩm')
@section('content')
    <div class="inner-header">
        <div class="container">
            <div class="pull-left">
                <h6 class="inner-title">Sản phẩm</h6>
            </div>
            <div class="pull-right">
                <div class="beta-breadcrumb font-large">
                    <a href="{{route('trang-chu')}}">Trang chủ</a> / <span>Sản phẩm</span>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="container">
        <div id="content" class="space-top-none">
            <div class="main-content">
                {{--<div class="space60">&nbsp;</div>--}}
                <div class="row">
                    <div class="col-sm-3">

                        <div class="panel panel-primary">
                            <div class="panel-heading"><h5>Danh mục</h5></div>
                            <div class="panel-body">
                                <ul class="aside-menu">
                                    @foreach($loai as $item)
                                        <li><a href="#" class="cate" cate_id="{{$item->id}}" cate_name="{{$item->name}}">{{$item->name}}</a></li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        <div class="panel panel-primary">
                            <div class="panel-heading"><h5>Nhãn hiệu</h5></div>
                            <div class="panel-body">
                                <form action="" method="get" id="formfilter" >
                                    {{csrf_field()}}
                                    <input type="hidden" id="type_productt" value="{{$loai_ten->id}}" name="type_product">
                                    <input type="hidden" id="brand1" value="0" name="brand[]">
                                    <input type="hidden" id="brand2" value="0" name="brand[]">
                                    <input type="hidden" id="brand3" value="0" name="brand[]">
                                    <input type="hidden" id="price_filter1" value="0" name="price_min">
                                    <input type="hidden" id="price_filter2" value="0" name="price_max">
                                </form>
                                <ul class="aside-menu">

                                     @foreach($brand as $item)
                                        <li><a href="#" class="brand" brand_id="{{$item->id}}">{{$item->name}}</a><span><img class="chuv chuv{{$item->id}}" imgid="{{$item->id}}" src="image/chuv.png" width="15px" alt=""></span></li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        <div class="panel panel-primary">
                                <div class="panel-heading"><h5>Giá</h5></div>
                                <div class="panel-body">
                                    <ul class="aside-menu">
                                        <li class="filter_price" val_min="20" val_max="99" >
                                            <a href="#" ><small class="price">Nhỏ hơn 100,000 vnđ</small></a>
                                        </li>
                                        <li class="filter_price"  val_min="100" val_max="300" >
                                            <a href="#"><small class="price"> Từ 100,000 vnđ - 300,000 vnđ</small></a>
                                        </li>
                                        <li class="filter_price"  val_min="300" val_max="500" >
                                            <a href="#"><small class="price"> Từ 300,000 vnđ - 500,000 vnđ</small></a>
                                        </li>
                                        <li class="filter_price"  val_min="500" val_max="0" >
                                            <a href="#"><small class="price">Trên 500,000 vnđ</small></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                    </div>
                    <div class="col-sm-9">
                        <div class="beta-products-list">
                            <h4 class="cate_name" >{{$loai_ten->name}}</h4>
                            <div class="space60">&nbsp;</div>
                            <div class="row productss">
                            @foreach($loaisp as $item)
                                <div class="col-sm-4">
                                    <div class="single-item">
                                        @if($item->promotion_price!=0)
                                            <div class="ribbon-wrapper"><div class="ribbon sale">Sale</div></div>
                                        @endif
                                        <div class="single-item-header">
                                            <a href="{{route('chitietsanpham',[$item->id,$item->alias])}}"><img src="uploads/product/{{$item->image}}" height="250px" alt=""></a>
                                        </div>
                                        <div class="single-item-body">
                                            <p class="single-item-title">{{$item->name}}</p>
                                            <p class="single-item-price">
                                                @if($item->promotion_price==0)
                                                    <span>{{number_format($item->unit_price,3,',','.')}}vnđ</span>
                                                @else
                                                    <span class="flash-del">{{number_format($item->unit_price,3,',','.')}}vnđ</span>
                                                    <span class="flash-sale">{{number_format($item->promotion_price,3,',','.')}}vnđ</span>
                                                @endif
                                            </p>
                                        </div>
                                        <div class="single-item-caption">
                                            <a class="add-to-cart pull-left" href="{{route('themgiohang',$item->id)}}"><i class="fa fa-shopping-cart"></i></a>
                                            <a class="beta-btn primary" href="{{route('chitietsanpham',[$item->id,$item->alias])}}">Chi tiết<i class="fa fa-chevron-right"></i></a>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>
                                    @endforeach()
                                <div class="pull-right">{{$loaisp->links()}}</div></div>
                            </div>
                            <div class="row">

                        </div> <!-- .beta-products-list -->

                        <div class="space50">&nbsp;</div>
                    </div>
                </div> <!-- end section with sidebar and main content -->


            </div> <!-- .main-content -->
        </div> <!-- #content -->
    </div>
    @endsection
