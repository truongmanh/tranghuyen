@foreach($loaisp as $item)
<div class="col-sm-4">
    <div class="single-item">
        @if($item->promotion_price!=0)
            <div class="ribbon-wrapper"><div class="ribbon sale">Sale</div></div>
        @endif
        <div class="single-item-header">
            <a href="{{route('chitietsanpham',[$item->id,$item->alias])}}"><img src="uploads/product/{{$item->image}}" height="250px" alt=""></a>
        </div>
        <div class="single-item-body">
            <p class="single-item-title">{{$item->name}}</p>
            <p class="single-item-price">
                @if($item->promotion_price==0)
                    <span>{{number_format($item->unit_price,3,',','.')}}vnđ</span>
                @else
                    <span class="flash-del">{{number_format($item->unit_price,3,',','.')}}vnđ</span>
                    <span class="flash-sale">{{number_format($item->promotion_price,3,',','.')}}vnđ</span>
                @endif
            </p>
        </div>
        <div class="single-item-caption">
            <a class="add-to-cart pull-left" href="{{route('themgiohang',$item->id)}}"><i class="fa fa-shopping-cart"></i></a>
            <a class="beta-btn primary" href="{{route('chitietsanpham',[$item->id,$item->alias])}}">Chi tiết<i class="fa fa-chevron-right"></i></a>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
    @endforeach()
<div class="pull-right">{{$loaisp->links()}}</div></div>