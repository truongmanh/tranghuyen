<!doctype html>
<html lang="en">
<head>
    <meta property="fb:admins" content="340870602991877"/>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('title')</title>
    <link rel="shortcut icon" type="image/x-icon" href="/favicon.png">
    <base href="{{ asset('') }}">

    <link href='http://fonts.googleapis.com/css?family=Dosis:300,400' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/dest/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/dest/vendors/colorbox/example3/colorbox.css">
    <link rel="stylesheet" href="assets/dest/rs-plugin/css/settings.css">
    <link rel="stylesheet" href="assets/dest/rs-plugin/css/responsive.css">
    <link rel="stylesheet" title="style" href="assets/dest/css/style.css?v=<?=time()?>">
    <link rel="stylesheet" href="assets/dest/css/animate.css">
    <link rel="stylesheet" title="style" href="assets/dest/css/huong-style.css?v=<?=time()?>">
</head>
<body>
<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.11&appId=127470717948505';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>

@include('site.header')
@yield('content')
<!-- .container -->
@include('site.footer')


<!-- include js files -->

<script src="assets/dest/js/jquery.js?v=<?=time()?>"></script>
<script src="assets/dest/vendors/jqueryui/jquery-ui-1.10.4.custom.min.js"></script>
<script src="http://netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
<script src="assets/dest/vendors/bxslider/jquery.bxslider.min.js"></script>
<script src="assets/dest/vendors/colorbox/jquery.colorbox-min.js"></script>
<script src="assets/dest/vendors/animo/Animo.js"></script>
<script src="assets/dest/vendors/dug/dug.js"></script>
<script src="assets/dest/js/scripts.min.js?v=<?=time()?>"></script>
<script src="assets/dest/js/jquery.validate.js"></script>
<script src="assets/dest/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
<script src="assets/dest/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
<script src="assets/dest/js/waypoints.min.js"></script>
<script src="assets/dest/js/wow.min.js"></script>
<!--customjs-->
<script src="assets/dest/js/custom2.js?v=<?=time()?>"></script>
<script>
    $(document).ready(function($) {
        $(window).scroll(function(){
            if($(this).scrollTop()>150){
                $(".header-bottom").addClass('fixNav')
            }else{
                $(".header-bottom").removeClass('fixNav')
            }}
        )

        $("body").delegate(".add-wishlist","click",function (e) {
            e.preventDefault();
            var id=$(this).val();
            $.ajax({
                type: "Get",
                url:'/addwishlist/'+id,
                success: function(data) {
                    if(data.message){
                        c=data.count;
                        $("#like-btn").html(
                            '<button class="btn btn-primary btn-huy" value="'+id+'">Hủy quan tâm</button>'
                        );
                        $("body").find("#view_count"+id).html(
                            'Quan tâm:<span style="color: red">'+c.view_count+'</span>'
                        );
                    }
                    if(data.errors){
                        alert(data.errors);

                    }
                }
            });
        });

        // ajax product

        $("body").on("click",".cate",function (e) {
            e.preventDefault();
            // $("#formfilter")[0].reset();
            var id=$(this).attr('cate_id');
            var name=$(this).attr('cate_name');
            $(".cate_name").html(name);
            $("#type_productt").val(id);

            $("li.filter_price").removeClass("show_price");
            $(".chuv").css("display","none");

            $("#price_filter1").val(0);
            $("#price_filter2").val(0);
            $("#brand1").val(0);
            $("#brand2").val(0);
            $("#brand3").val(0);
            // alert(id);
            $.ajax({
                url:"/ajaxproduct/"+id,
                type:"get",
                dataType:"html"
            }).done(function(data){
                $(".productss").html(data);
            }).fail(function(erro){
                console.log(erro.statusText);
            });
        });



        function getdata(dataform) {
            $.ajax({
                url:"/brandproduct",
                type:"get",
                data:dataform,
                dataType:'html'
            }).done(function(data){
                $(".productss").html(data);
            }).fail(function(erro){
                console.log(erro.statusText);
            });
        }



$("body").on("click","li.filter_price",function (e) {
    e.preventDefault();
    $("li.filter_price").removeClass("show_price");
    $(this).addClass("show_price");

    var p_min=$(this).attr("val_min");
    var p_max=$(this).attr("val_max");
    $("#price_filter1").val(p_min);
    $("#price_filter2").val(p_max);
    var dataform=$("#formfilter").serialize();
    console.log(dataform);
    getdata(dataform);
})

$("body").on("click",".brand",function (e) {
   e.preventDefault();
   var id=$(this).attr("brand_id");
   $(".chuv"+id).css("display","block");
   $("#brand"+id).val(id);
   var dataform=$("#formfilter").serialize();
   console.log(dataform);
    getdata(dataform);

});

$("body").on("click",".chuv",function (e) {
    e.preventDefault();
    $(this).css("display","none");
   var id =$(this).attr("imgid");
    $("#brand"+id).val(0);
    var dataform=$("#formfilter").serialize();
    console.log(dataform);
    getdata(dataform);
});

        $("body").delegate(".btn-huy","click",function (e) {
            e.preventDefault();
            var id=$(this).val();
            $.ajax({
                type: "Get",
                url:'/delwishlist/'+id,
                success: function(data) {
                    c=data.count;
                    if(data.message){
                        $("#like-btn").html(
                            '<button class="add-wishlist like-btn" value="'+id+'"><i class="fa fa-heart"></i></button>'
                        );
                        $("body").find("#view_count"+id).html(
                            'Quan tâm:<span style="color: red">'+c.view_count+'</span>'
                        );
                    }
                }
            });
        });

        $(".errors-login").delay(3000).slideUp("slow")

    });
    $('.alert').delay(3000).slideUp();


// VALIDATE DANG KY

$("#formdk").validate({
//specify the validation rules
rules: {
name: "required",
pass: "required",
email: {
    required: true,
    email: true     
},

},
 
//specify validation error messages
messages: {
name: "Tên không được để trống",
pass: "Mật khẩu không được để trống",
email:{
    required:"Email không được để trống",
    email:"Email không đúng định dạng"
}
},
 
submitHandler: function(form){
form.submit();
}
 
});

// VALIDATE DANG NHAP


$("#formdn").validate({
//specify the validation rules
rules: {
name: "required",
pass: "required",

},
 
//specify validation error messages
messages: {
name: "Tên không được để trống",
pass: "Mật khẩu không được để trống",
},
 
submitHandler: function(form){
form.submit();
}
 
});

// VALIDATE GUI MAIL

$("#formlh").validate({
//specify the validation rules
rules: {
message: "required",
subject: "required",
email:{
    required: true,
    email: true   
}
},
 
//specify validation error messages
messages: {
message: "nội dung không được để trống",
subject: "Chủ đề không được để trống",
email:{
    required:"Email không được để trống",
    email:"Email không đúng định dạng"
}
},
 
submitHandler: function(form){
form.submit();
}
 
});

//VALIDATE DAT HANG
    $("#form_dathang").validate({
//specify the validation rules
        rules: {
            name: "required",
            gender: "required",
            adress: "required",
            phone: {
                required: true,
                number:true,
                minlength:6,
                maxlength:11,
            },
            email:{
                required: true,
                email: true
            }
        },

//specify validation error messages
        messages: {
            name: "Tên không được để trống",
            gender: "giới tính không được để trống",
            adress:"Địa chỉ không được để trống",
            phone:{
                required: "Số điện thoại không được để trống",
                number:"Số điện thoại không đúng định dạng",
                minlength:"Số điện thoại quá ngắn",
                maxlength:"Số điện thoại quá dài",
            },
            email:{
                required:"Email không được để trống",
                email:"Email không đúng định dạng"
            }
        },

        submitHandler: function(form){
            form.submit();
        }

    });



</script>

</body>
</html>
