@extends('admin.master')
@section('title','Danh sách khách hàng')
@section('content')
    @if(Session::has('flash_message'))
        <div class="alert alert-danger">
            {{Session::get('flash_message')}}
        </div>
        @endif
    <section class="content-header">
        <h1>
            Quản lí khách hàng
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Trang chủ</a></li>
            <li class="active">Danh sách khách hàng</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Danh sách khách hàng</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table class="table table-bordered">
                    <tbody><tr>
                        <th style="width: 10px">STT</th>
                        <th>Tên</th>
                        <th>Email</th>
                        <th>Địa chỉ</th>
                        <th>Số điện thoại</th>
                        {{--<th style="width: 40px">Label</th>--}}
                    </tr>
                    <?php $stt=1?>
                    @foreach($customer as $item)
                        <tr>
                            <td>{{$stt++}}</td>
                            <td>{{$item->name}}</td>    
                            <td>{{$item->email}}</td>
                            <td>
                                    {{$item->address}}
                            </td>
                            <td>
                                    {{$item->phone_number}}
                            </td>
                            
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
                <ul class="pagination pagination-sm no-margin pull-right">
                    {{ $customer->links() }}
                </ul>
            </div>
        </div>

        <!-- /.row -->
    </section>

    @endsection