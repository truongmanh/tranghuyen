@extends('admin.master')
@section('title','Thêm danh mục')
@section('content')
    @if(count($errors)>0)
        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <section class="content-header">
        <h1>
            Quản lí danh mục
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Trang chủ</a></li>
            <li class="active">Thêm danh mục</li>
        </ol>
    </section>
    <section class="content">
       <div class="row">
           <div class="col-md-8 col-md-offset-2">
               <div class="box box-primary">
                   <div class="box-header with-border">
                       <h3 class="box-title">Thêm danh mục</h3>
                   </div>
                   <form role="form" action="{{route('postAddCate')}}" method="post" enctype="multipart/form-data">
                       {{csrf_field()}}
                       <div class="box-body">
                           <div class="form-group">
                               <label for="txt_name">Danh mục cha</label>
                                   <select class="form-control" name="slt_name">
                                       <option value="0">Root</option>
                                       <?php Menu_mutil($data)?>
                                   </select>
                           </div>
                           <div class="form-group">
                               <label for="">Tên danh mục</label>
                               <input type="text" class="form-control" name="txt_name" placeholder="tên danh mục">
                           </div>
                           <div class="form-group">
                               <label for="">Mô tả</label>
                               <textarea class="form-control" name="txt_des" placeholder="description"></textarea>
                           </div>
                           <div class="form-group">
                               <label for="">Trạng thái</label>
                               <select class="form-control" name="slt_status">
                                   <option value="1">Hiện</option>
                                   <option value="0">Ẩn</option>
                               </select>
                           </div>
                           <div class="form-group">
                               <label for="">Icon</label>
                               <input type="file" name="txt_img" id="txt_img" placeholder="name">
                           </div>
                       </div>
                       <!-- /.box-body -->

                       <div class="box-footer">
                           <a href="{{route('getListCate')}}"><button type="button" class="btn btn-danger pull-right " style="margin-left: 5px">Cancel</button></a>
                           <button type="submit" class="btn btn-primary pull-right">Thêm danh mục</button>
                       </div>
                   </form>
               </div>
           </div>
       </div>
    </section>
@endsection