@extends('admin.master')
@section('title','Danh sách sản phẩm')
@section('content')
    @if(Session::has('flash_message'))
        <div class="alert alert-danger">
            {{Session::get('flash_message')}}
        </div>
    @endif
    <section class="content-header">
        <h1>
            Quản lí sản phẩm
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Trang chủ</a></li>
            <li class="active">Danh sách sản phẩm</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Danh sách sản phẩm</h3>
            </div>
          <a href="{{route('getAddProduct')}}" class="pull-right" style="margin-right:15px">  <button class="btn btn-primary">Thêm sản phẩm</button></a>
            <!-- /.box-header -->
            <div class="box-body">
                <table class="table table-bordered">
                    <tbody><tr>
                        <th style="width: 10px">STT</th>
                        <th>Tên</th>   
                        <th>Hình ảnh</th>
                        <th>Mô tả</th>
                        <th>Trạng thái</th>
                        <th>Hành động</th>
                        {{--<th style="width: 40px">Label</th>--}}
                    </tr>
                    <?php $stt=1?>
                    @foreach($data as $item)
                        <tr>
                            <td>{{$stt++}}</td>
                            <td>{{$item->name}}</td>
                            <td><img src='uploads/product/{{$item->image}}' width='80px'/></td>
                            <td width='300px'>{!!$item->description!!}</td>
                            <td>
                                @if($item->status==0)
                                    {{"Ẩn"}}
                                @else
                                    {{"Hiện"}}
                                @endif
                            </td>
                            <td>
                                <a href="{{route('getEditProduct',['id'=>$item->id])}}"><button type="button" class="btn btn-success btn-sm">Sửa</button></a> <a href="{{route('getDelProduct',['id'=>$item->id])}}"><button type="button" class="btn btn-danger btn-sm">Xóa</button></a></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <!-- /box-body -->
            <div class="box-footer clearfix">
                <ul class="pagination pagination-sm no-margin pull-right">
                    {{ $data->links() }}
                </ul>
            </div>
        </div>

        <!-- /.row -->
    </section>
@endsection