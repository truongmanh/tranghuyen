@extends('admin.master')
@section('title','Danh sách đơn hàng')
@section('content')
    @if(Session::has('flash_message'))
        <div class="alert alert-danger">
            {{Session::get('flash_message')}}
        </div>
        @endif
    <section class="content-header">
        <h1>
            Quản lí đơn hàng
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Trang chủ</a></li>
            <li class="active">Danh sách đơn hàng</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
    <div class="row bill">
        <div class="col-md-4">
            <div class="info-box bg-aqua">
                <span class="info-box-icon"><i class="fa fa-bookmark-o"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Tổng đơn hàng đã nhập:{{count($data_bill_all)}}</span>
                    <span class="info-box-number">Tổng tiền:{{number_format($total_bill_all,3,',','.')}}vnđ</span>
                    <div class="progress">
                        <div class="progress-bar" style="width: 100%"></div>
                    </div>
                    <span class="progress-description">
                       <span class="info-box-text">Tổng đơn hàng đã bán:{{count($data_bill)}}</span>
                    <span class="info-box-number">Tổng tiền:{{number_format($total_bill,3,',','.')}}vnđ</span>
                  </span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <div class="col-md-4">
            <div class="info-box bg-green">
                <span class="info-box-icon"><i class="fa fa-thumbs-o-up"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Tổng hàng đã nhập trong tháng:{{count($bill_thang_all)}}</span>
                    <span class="info-box-number">Tổng tiền:{{number_format($total_bill_thang_all,3,',','.')}}vnđ</span>

                    <div class="progress">
                        <div class="progress-bar" style="width: 100%"></div>
                    </div>
                    <span class="progress-description">
                            <span class="info-box-text">Tổng đơn hàng trong tháng:{{count($bill_thang)}}</span>
                    <span class="info-box-number">Tổng tiền:{{number_format($total_bill_thang,3,',','.')}}vnđ</span>
                  </span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <div class="col-md-4">
            <div class="info-box bg-yellow">
                <span class="info-box-icon"><i class="fa fa-calendar"></i></span>

                <div class="info-box-content">
                        <span class="info-box-text">Tổng đơn hàng hôm nay:{{count($bill_today)}}</span>
                        <span class="info-box-number">Tổng tiền:{{number_format($total_bill_today,3,',','.')}}vnđ</span>

                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>

    </div>
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Danh sách đơn hàng chưa thanh toán</h3>
            </div>
            {{--<form action="#">--}}
                {{--<div class="form-group">--}}
                    {{--<div class="col-md-3 pull-right" style="margin-bottom: 10px">--}}
                        {{--<select class="form-control" name="sltpay" id="sltpay">--}}
                            {{--<option>--Chọn--</option>--}}
                            {{--<option value="1">Đã thanh toán</option>--}}
                            {{--<option value="0">Chưa thanh toán</option>--}}
                        {{--</select>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</form>--}}
            <!-- /.box-header -->
            <div class="box-body">
                <table class="table table-bordered">
                    <tbody><tr>
                        <th style="width: 10px">STT</th>
                        <th>Tên</th>
                        <th>Ngày</th>
                        <th>Tổng tiền</th>
                        <th>Hình thức</th>
                        <th>Trạng thái</th>
                        <th>Hành động</th>
                        {{--<th style="width: 40px">Label</th>--}}
                    </tr>
                    <?php $stt=1?>
                    @foreach($data as $item)
                        <tr>
                            <td>{{$stt++}}</td>
                            <td>{{$item->namecus}}</td>    
                            <td>{{$item->date_order}}</td>
                            <td>
                                    {{$item->total}},000vnđ
                            </td>
                            <td>
                                    {{$item->payment}}
                            </td>
                            <td>
                                @if($item->status ==1)
                                    Đã thanh toán
                                @else
                                    Chưa thanh toán
                                @endif
                            </td>
                            <td>
                               <a href="{{route('getEditBill',['id'=>$item->id])}}"> <button type="button" class="btn btn-success btn-sm">Sửa</button></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
                <ul class="pagination pagination-sm no-margin pull-right">
                    {{ $data->links() }}
                </ul>
            </div>
        </div>
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Danh sách đơn hàng đã thanh toán</h3>
            </div>
        {{--<form action="#">--}}
        {{--<div class="form-group">--}}
        {{--<div class="col-md-3 pull-right" style="margin-bottom: 10px">--}}
        {{--<select class="form-control" name="sltpay" id="sltpay">--}}
        {{--<option>--Chọn--</option>--}}
        {{--<option value="1">Đã thanh toán</option>--}}
        {{--<option value="0">Chưa thanh toán</option>--}}
        {{--</select>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--</form>--}}
        <!-- /.box-header -->
            <div class="box-body">
                <table class="table table-bordered">
                    <tbody><tr>
                        <th style="width: 10px">STT</th>
                        <th>Tên</th>
                        <th>Ngày</th>
                        <th>Tổng tiền</th>
                        <th>Hình thức</th>
                        <th>Trạng thái</th>
                        {{--<th>Hành động</th>--}}
                        {{--<th style="width: 40px">Label</th>--}}
                    </tr>
                    <?php $stt=1?>
                    @foreach($data_payed as $item)
                        <tr>
                            <td>{{$stt++}}</td>
                            <td>{{$item->namecus}}</td>
                            <td>{{$item->date_order}}</td>
                            <td>
                                {{$item->total}},000vnđ
                            </td>
                            <td>
                                {{$item->payment}}
                            </td>
                            <td>
                                @if($item->status ==1)
                                    Đã thanh toán
                                @else
                                    Chưa thanh toán
                                @endif
                            </td>
                            {{--<td>--}}
                                {{--<a href="{{route('getEditBill',['id'=>$item->id])}}"> <button type="button" class="btn btn-success btn-sm">Sửa</button></a>--}}
                            {{--</td>--}}
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
                <ul class="pagination pagination-sm no-margin pull-right">
                    {{ $data_payed->links() }}
                </ul>
            </div>
        </div>
        <!-- /.row -->
    </section>

    @endsection