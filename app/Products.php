<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    protected $table="product";
    protected $fillable=[
        'name','id_type','description','unit_price','promotion_price','image','new','status'
    ];

    public function TypeProduct(){
        return $this->belongsTo('App\type_product','id_type','id');
    }

    public function BillDetail(){
        return $this->hasMany('App\BillDetail','id_product','id');
    }
}
