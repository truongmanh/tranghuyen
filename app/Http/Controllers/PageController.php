<?php

namespace App\Http\Controllers;

use App\Bill;
use App\BillDetail;
use App\Brand;
use App\Cart;
use App\Customer;
use App\Products;
use App\Slider;
use App\type_product;
use App\User;
use Illuminate\Http\Request;
use Session;
use Hash;
use Illuminate\Support\Facades\Auth;
use DB;
use Mail;

class PageController extends Controller
{
 
    public function getIndex(){
    
        $new_product=Products::where('new',1)->where('status',1)->paginate(8);
        $sale_product=Products::where('promotion_price','<>',0)->paginate(8);
        $hot_product=Products::orderBy('buy_count','desc')->take(8)->get();
        return view("site.page.trangchu",compact('silde','new_product','sale_product','hot_product'));
    }
    public function getTypeSp($id){
    
        $loaisp=Products::where('product.id_type',$id)->paginate(9);
        $loai_ten=type_product::find($id);
        $loai=type_product::all();
        $brand=Brand::all();
        return view ('site.page.loai_san_pham',compact('loaisp','loai_ten','loai','brand'));
    }

    public function getAjaxProduct(Request $request){
        $loaisp=Products::where('id_type',$request->id)->paginate(20);
        return view ('site.layout.ajaxproduct',compact('loaisp'));
    }
    public function getBrandProduct(Request $request){

        try{
            $data=[];
            for($i=0;$i<count($request->brand);$i++){
                if($request->brand[$i]!=0){
                   array_push($data,$request->brand[$i]);
                }
            }
            if(empty($data)){
            if($request->price_min !=0 && $request->price_max!=0){
                $loaisp=Products::where('id_type',$request->type_product)
                    ->whereBetween('unit_price',[$request->price_min,$request->price_max])
                    ->paginate(20);
                return view ('site.layout.ajaxproduct',compact('loaisp'));
            }
                elseif ($request->price_min!=0 && $request->price_max==0){

                    $loaisp=Products::where('id_type',$request->type_product)
                    ->where('unit_price','>',$request->price_min)
                    ->paginate(20);
                    return view ('site.layout.ajaxproduct',compact('loaisp'));
                }

                 else{
                     $loaisp=Products::where('id_type',$request->type_product)
                         ->paginate(20);
                     return view ('site.layout.ajaxproduct',compact('loaisp'));
                 }
            }
            else{

    if($request->price_min !=0 && $request->price_max!=0){

        $loaisp=Products::whereIn('brand_id',$request->brand)
            ->where('id_type',$request->type_product)
            ->whereBetween('unit_price',[$request->price_min,$request->price_max])
            ->paginate(20);
        return view ('site.layout.ajaxproduct',compact('loaisp'));

    }
    elseif ($request->price_min!=0 && $request->price_max==0){

        $loaisp=Products::whereIn('brand_id',$request->brand)
            ->where('id_type',$request->type_product)
            ->where('unit_price','>',$request->price_min)
            ->paginate(20);
        return view ('site.layout.ajaxproduct',compact('loaisp'));

    }

    else{
        $loaisp=Products::where('id_type',$request->type_product)
            ->whereIn('brand_id',$request->brand)
            ->paginate(20);
        return view ('site.layout.ajaxproduct',compact('loaisp'));

    }
            }

        }
        catch(\Exception $e){
            return $e->getMessage();
        }

    }

    public function getDetailSp(Request $req){
        $detail=Products::where('id',$req->id)->first();
        $spsame=Products::where('id_type',$detail->id_type)->inRandomOrder()->take(3)->get();
        $sale_product=Products::where('promotion_price','<>',0)->take(6)->get();
        $detail_star=Products::select('buy_count','view_count')->where('id',$req->id)->get();
       if(Auth::check()){
           $data_wish=DB::table('wishlist_users')
               ->join('product','product.id','=','wishlist_users.id_product')
               ->where('wishlist_users.id_product','=',$req->id)
               ->where('wishlist_users.id_user','=',Auth::user()->id)
               ->first();
           if($data_wish){
               $data_w="yes";
           }
           else{
               $data_w="no";
           }
       }
       else{
           $data_w="no";
       }
//        dd($data_w);
        return view ('site.page.chi_tiet_san_pham',compact('detail','spsame','data_w','sale_product','detail_star'));
    }

    public function getAddCart(Request $req,$id){
        $product=Products::find($id);
        $oldCart=Session('cart')?Session::get('cart'):null;
        $cart=new Cart($oldCart);
        $cart->add($product,$id);
        $req->Session()->put('cart',$cart);
        return redirect()->back();
    }

//    xóa 1 sp trong giỏ hàng
    public function getDelCartOne($id){
        $product=Products::find($id);
        $oldCart=Session::has('cart')?Session::get('cart'):null;
        $cart=new Cart($oldCart);
        $cart->reduceByOne($product,$id);
        if(count($cart->items)>0){
            Session::put('cart',$cart);
        }
        else{
            Session::forget('cart');
        }
        return redirect()->back();
    }
//    xóa giỏ hàng
    public function getDelCart($id){
        $oldCart=Session::has('cart')?Session::get('cart'):null;
        $cart=new Cart($oldCart);
        $cart->removeItem($id);
        if(count($cart->items)>0){
            Session::put('cart',$cart);
        }
        else{
            Session::forget('cart');
        }
        return redirect()->back();
    }


    public function getContact(){

        return view ('site.page.lienhe');
    }

    public function postContact(Request $request){
        $data=[
          'email'=>$request->email,
          'subject'=>$request->subject,
          'content'=>$request->message
        ];
        Mail::send('mail',$data,function ($message) use ($data){
            $message->to('truongmanh93cntt@gmail.com')->subject($data['subject']);
            $message->from($data['email']);
        });
        return redirect()->route('trang-chu')->with(['mailmessage'=>'Bạn đã gửi mail thành công']);
    }


    public function getdangky(){
        return view('site.page.dangky');
    }

    public function postdangky(Request $request){

            $user=new User();
            $user->name=$request->name;
            $user->email=$request->email;
            $user->password=Hash::make($request->pass);
            $user->role=2;
            $user->status=1;
            $user->save();
            return redirect()->back()->with('thong bao','Tạo tài khoản thành công');
    }


    public function getdangnhap(){
        return view('site.page.dangnhap');
    }

    public function postdangnhap(Request $request){
        $login=[
          'name'=>$request->name,
          'password'=>$request->pass,
            'role'=>2,
            'status'=>1
        ];
        if (Auth::attempt(['name'=>$request->name,'password'=>$request->pass]))
        {
            return redirect()->route("trang-chu");
        }
        else{
            return redirect()->back()->with('thong bao','Đăng nhập thất bại,xin vui lòng nhập đúng tên hoặc mật khẩu');
        }
    }

    public function postDangxuat(){
        Auth::logout();
        return redirect()->route("trang-chu");
    }

    public function getTimKiem(Request $req){
        $hot_product=Products::orderBy('buy_count','desc')->take(8)->get();
        $silde=Slider::all();
        $product=Products::where('name','like','%'.$req->txtsearch.'%')->paginate(8);
        return view('site.page.timkiem',compact('silde','product','hot_product'));
    }

    public function getGioithieu(){
        return view('site.page.gioithieu');
    }


    public function getCheckout(){
        if(Auth::check()){
            $data_cuss=Customer::where('email','=',Auth::user()->email)
                ->first();
           if($data_cuss){
               $data=$data_cuss;
               return view('site.page.dathang',compact('data'));
           }
           else{
               $data=User::find(Auth::user()->id);
               return view('site.page.dathang',compact('data'));
           }
        }
        else{
            return view('site.page.dathang');
        }

    }
    public function postCheckout(Request $request){

              $cart=Session::get('cart');
             if(Auth::check()){
                 $data_cuss=Customer::where('email','=',Auth::user()->email)
                     ->first();
                 if($data_cuss){
                     $data_cuss->name=$request->name;
                     $data_cuss->gender=$request->gender;
                     $data_cuss->phone_number=$request->phone;
                     $data_cuss->address=$request->adress;
                     $data_cuss->email=$request->email;
                     $data_cuss->note=$request->note;
                     $data_cuss->update();

                     $bill=new Bill();
                     $bill->id_customer= $data_cuss->id;
                     $bill->date_order=date('Y-m-d');
                     $bill->total=$cart->totalPrice;
                     $bill->payment=$request->payment_method;
                     $bill->note=$request->note;
                     $bill->status="0";
                     $bill->save();


                     foreach ($cart->items as $key=>$value){
                         $bill_detail=new BillDetail();
                         $bill_detail->id_bill=$bill->id;
                         $bill_detail->id_product=$key;
                         $bill_detail->quantity=$value['qty'];
                         $bill_detail->unit_price=$value['price']/$value['qty'];
                         $bill_detail->save();

                         $data=Products::findOrFail($key);
                         $data->increment('buy_count');
                     }

                     Session::forget('cart');
                 }
                 else{
                     $customer=new Customer();
                     $customer->name=$request->name;
                     $customer->gender=$request->gender;
                     $customer->phone_number=$request->phone;
                     $customer->address=$request->adress;
                     $customer->email=$request->email;
                     $customer->note=$request->note;
                     $customer->save();

                     $user1=User::find(Auth::user()->id);
                     $user1->id_cus= $customer->id;
                     $user1->update();

                     $bill=new Bill();
                     $bill->id_customer= $customer->id;
                     $bill->date_order=date('Y-m-d');
                     $bill->total=$cart->totalPrice;
                     $bill->payment=$request->payment_method;
                     $bill->note=$request->note;
                     $bill->status="0";
                     $bill->save();


                     foreach ($cart->items as $key=>$value){
                         $bill_detail=new BillDetail();
                         $bill_detail->id_bill=$bill->id;
                         $bill_detail->id_product=$key;
                         $bill_detail->quantity=$value['qty'];
                         $bill_detail->unit_price=$value['price']/$value['qty'];
                         $bill_detail->save();

                         $data=Products::findOrFail($key);
                         $data->increment('buy_count');
                     }

                     Session::forget('cart');
                 }

             }
             else{
                 $data_cus=Customer::where('email','=',$request->email)
                     ->first();
                 if(!$data_cus){
                     $customer=new Customer();
                     $customer->name=$request->name;
                     $customer->gender=$request->gender;
                     $customer->phone_number=$request->phone;
                     $customer->address=$request->adress;
                     $customer->email=$request->email;
                     $customer->note=$request->note;
                     $customer->save();

                     $user1=new User();
                     $user1->name=$request->name;
                     $user1->email=$request->email;
                     $user1->password=Hash::make(123456);
                     $user1->role=2;
                     $user1->status=1;
                     $user1->id_cus= $customer->id;
                     $user1->save();

                     $bill=new Bill();
                     $bill->id_customer=$customer->id;
                     $bill->date_order=date('Y-m-d');
                     $bill->total=$cart->totalPrice;
                     $bill->payment=$request->payment_method;
                     $bill->note=$request->note;
                     $bill->status="0";
                     $bill->save();


                     foreach ($cart->items as $key=>$value){
                         $bill_detail=new BillDetail();
                         $bill_detail->id_bill=$bill->id;
                         $bill_detail->id_product=$key;
                         $bill_detail->quantity=$value['qty'];
                         $bill_detail->unit_price=$value['price']/$value['qty'];
                         $bill_detail->save();

                         $data=Products::findOrFail($key);
                         $data->increment('buy_count');
                     }

                     Session::forget('cart');
                     $login=[
                         'name'=>$request->name,
                         'password'=>'123456',
                         'role'=>2,
                         'status'=>1
                     ];
                     Auth::attempt($login);
                 }
                 else{

                     $bill=new Bill();
                     $bill->id_customer=$data_cus->id;
                     $bill->date_order=date('Y-m-d');
                     $bill->total=$cart->totalPrice;
                     $bill->payment=$request->payment_method;
                     $bill->note=$request->note;
                     $bill->status="0";
                     $bill->save();


                     foreach ($cart->items as $key=>$value){
                         $bill_detail=new BillDetail();
                         $bill_detail->id_bill=$bill->id;
                         $bill_detail->id_product=$key;
                         $bill_detail->quantity=$value['qty'];
                         $bill_detail->unit_price=$value['price']/$value['qty'];
                         $bill_detail->save();

                         $data=Products::findOrFail($key);
                         $data->increment('buy_count');
                     }

                     Session::forget('cart');
                     $data_u=DB::table('users')
                         ->join('customer','customer.id','=','users.id_cus')
                         ->first();
                     $login=[
                         'name'=>$data_u->name,
                         'password'=>$data_u->password,
                         'role'=>2,
                         'status'=>1
                     ];
                     Auth::attempt($login);
                 }
             }

                return redirect()->back()->with(['thong bao'=>'Đặt hàng thành công,chúng tôi sẽ giao hàng cho bạn trong thời gian nhanh nhất']);

    }
}
