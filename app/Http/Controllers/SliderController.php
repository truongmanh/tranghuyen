<?php

namespace App\Http\Controllers;

use App\Http\Requests\SliderRequest;
use App\Slider;
use Illuminate\Http\Request;
use File;

class SliderController extends Controller
{
  public function getListSlider(){
      $data=Slider::paginate(4);
      return view('admin.slider.list',compact('data'));
  }
  public function getAddSlider(){
      return view('admin.slider.add');
  }
  public function postAddSlider(SliderRequest $request){
      $slider=new Slider();
      $file=$request->file('txt_img');
      if(strlen($file)>0){
          $filename=time().'.'.$file->getClientOriginalExtension();
          $path="uploads/slider/";
          $file->move($path,$filename);
          $slider->link=$filename;
      }
      $slider->status=$request->slt_status;
      $slider->save();
      return redirect()->route('getListSlider')->with(['flash_message'=>'thêm ảnh thành công']);
  }
  public function getEditSlider($id){
        $data=Slider::findOrFail($id);
        return view('admin.slider.edit',compact('data'));
  }
  public function postEditSlider($id,Request $request){
      $slider=Slider::findOrFail($id);
      $file=$request->file('txt_img');
      if(strlen($file)>0){
          $img_current=$request->txt_img_current;
          if(file_exists(public_path().'/uploads/slider/'. $img_current)){
              File::delete(public_path().'/uploads/slider/'. $img_current);
          }
          $filename=time().'.'.$file->getClientOriginalExtension();
          $path="uploads/slider/";
          $file->move($path,$filename);
          $slider->link=$filename;
      }
      $slider->status=$request->slt_status;
      $slider->update();
      return redirect()->route('getListSlider')->with(['flash_message'=>'Sửa ảnh thành công']);
  }
    public function getDelSlider($id){
        $slider=Slider::findOrFail($id);
        if(file_exists(public_path().'/uploads/slider/'. $slider->link)){
            File::delete(public_path().'/uploads/slider/'. $slider->link);
        }
        $slider->delete();
        return redirect()->route('getListSlider')->with(['flash_message'=>'Xóa ảnh thành công']);
    }

}
