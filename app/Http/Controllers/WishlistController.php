<?php

namespace App\Http\Controllers;

use App\Products;
use App\WishUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
use App\Slider;
use Cache;
use App\WhishList;
use Illuminate\Support\Facades\Session;

class WishlistController extends Controller
{

    public function getIndex(){
        $silde=Slider::all();
        $hot_product=Products::orderBy('buy_count','desc')->take(8)->get();
       
       if(Auth::check()){
           $data= DB::table('product')
               ->join('wishlist_users','wishlist_users.id_product','=','product.id')
               ->where('wishlist_users.id_user','=',Auth::user()->id)
               ->select('product.*')
               ->get()->toArray();
       }
       else{
           $data=[];
//           if(Session::has('value')){
//               $list=Session::get('value');
//               $WhishList=new WhishList($list);
//               $data=$WhishList->items;
////               $data=$data['item'];
//                dd($data);
//           }
//            else{
//                $data=[];
//            }
       }

        return view('site.page.wishlist',compact('data','silde','hot_product'));
    }
    public function addWishlist($id,Request $request){
      if(Auth::check()){
          $data=Products::findOrFail($id);
          if($data){
              $data->increment('view_count');
              $data_wish= new WishUser();
              $data_wish->id_user=Auth::user()->id;
              $data_wish->id_product=$id;
              $data_wish->save();
              $data_view_count=Products::select('view_count')
                    ->where('id','=',$id)->first();
              return response()->json(
                  [
                      "message"=>"success",
                      "count"=>$data_view_count
                  ]
              );
          }
          else{
              return response()->json(
                  [
                      "message"=>"Lỗi"
                  ]
              );
          }
      }
      else{

//          $product=Products::find($id);
//          $product->increment('view_count');
//          $list=Session('value')?Session::get('value'):null;
//          $WhishList=new WhishList($list);
//          $WhishList->add($product,$id);
//          $request->Session()->put('value',$WhishList);
//          $data_view_count=Products::select('view_count')
//              ->where('id','=',$id)->first();
          return response()->json(
              [
                  "errors"=>"Bạn vui lòng đăng nhập để sử dụng chức năng này sản phẩm",
//                  "count"=>$data_view_count
              ]
          );
      }
    }

    public function delWishlist($id){
        $data=Products::findOrFail($id);
        if($data){
            $data->decrement('view_count');
           DB::table('wishlist_users')
               ->join('users','users.id','=','wishlist_users.id_user')
               ->join('product','product.id','=','wishlist_users.id_product')
               ->where('wishlist_users.id_user','=',Auth::user()->id)
               ->where('wishlist_users.id_product','=',$id)
               ->delete();
            $data_view_count=Products::select('view_count')
                ->where('id','=',$id)->first();
            return response()->json(
                [
                    "message"=>"success",
                    "count"=>$data_view_count
                ]
            );
        }
        else{
            return response()->json(
                [
                    "message"=>"Lỗi"
                ]
            );
        }
    }
}
