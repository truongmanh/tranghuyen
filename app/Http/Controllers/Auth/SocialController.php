<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Socialite;
use App\User;
use Hash;

class SocialController extends Controller

{
    public function redirectToProvider()
{
    return Socialite::driver('facebook')->redirect();
}

    public function handleProviderCallback()
    {
        try {
            $user = Socialite::driver('facebook')->user();

        } catch (Exception $e) {
            return redirect('auth/facebook');
        }
        $authUser = $this->findOrCreateUser($user);

        Auth::login($authUser, true);

        return redirect()->route('trang-chu');
    }

    private function findOrCreateUser($facebookUser)
    {
        $authUser = User::where('email',$facebookUser->email)->first();
        if ($authUser){
            return $authUser;
        }
        else{
            return User::create([
                'email' => $facebookUser->email,
                'name' => $facebookUser->name,
                'facebook_id' => $facebookUser->id,
                'password'=>Hash::make(123456),
            ]);
        }
    }
}
