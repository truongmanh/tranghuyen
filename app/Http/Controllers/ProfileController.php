<?php

namespace App\Http\Controllers;

use App\Http\Models\Customer;
use App\User;
use App\Products;
use App\Slider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;

class ProfileController extends Controller
{
   public function getIndex($id){
      if(Auth::check()){
          $data_cus=DB::table('customer')
              ->join('users','users.id_cus','=','customer.id')
              ->select('customer.id as id')
              ->where('users.id','=',$id)
              ->get()->toArray();
          if($data_cus){
              $data_cus=$data_cus[0]->id;
//          dd($data_cus);
              $data=DB::table('bill_detail')
                  ->join('bills','bills.id','=','bill_detail.id_bill')
                  ->join('product','product.id','=','bill_detail.id_product')
                  ->select('product.*','bill_detail.quantity','bill_detail.unit_price','bills.date_order')
                  ->where('bills.id_customer','=',$data_cus)
                  ->paginate(4);
          }
          else{
              $data=[];
          }

          $silde=Slider::all();
          $hot_product=Products::orderBy('buy_count','desc')->take(8)->get();
          return view('site.page.profile',compact('hot_product','silde','data'));
      }
      else{
          return redirect()->route('trang-chu');
      }
   }
}
