<?php

namespace App\Http\Controllers;

use App\Admin;
use App\Products;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Hash;
use App\Customer;
use App\Bill;
use DB;
use DateTime;
use Carbon\Carbon;

class AdminController extends Controller
{
    public function getIndex(){
          if(Auth::guard('admin')->check()){
            return redirect()->route('getListCate');
            }
           else{
              return  view('login.login');
          }
       }
    public function postLogin(Request $req){
        $name = $req->input('txt_name');
        $password = $req->input('txt_pass');

        if( Auth::guard('admin')->attempt(['name' => $name, 'password' =>$password,'role'=>1])) {
//            ECHO Auth::guard('admin')->user()->name;
            return redirect()->route('getListCate');
        } else {
            return redirect()->back()->with(['thong bao'=>'Nhập sai mật khẩu hoặc password']);
        }
    }
    public function getLogout(){
        Auth::guard('admin')->logout();
        return redirect('admin/login');
    }
    public function getListUser(){
      $data=User::select('id','name','email','role','status')->paginate(5);
      return view('admin.user.list',compact('data'));
    }
    public function getListAdmin(){
        $data=Admin::select('id','name','email','role','status')->paginate(5);
        return view('admin.user.list_admin',compact('data'));
    }

    public function getAddUser(){
       return view('admin.user.add');
    }

    public function postAddUser(Request $req){
        if ($req->slt_role=="1"){
            $admin=new Admin();
            $admin->name=$req->txt_name;
            $admin->email=$req->txt_email;
            $admin->password=Hash::make($req->txt_pass);
            $admin->role=$req->slt_role;
            $admin->status=$req->slt_status;
            $admin->save();
            return redirect()->route('getListAdmin')->with(['flash_message'=>'Thêm admin thành công']);
        }
        else{
            $user=new User();
            $user->name=$req->txt_name;
            $user->email=$req->txt_email;
            $user->password=Hash::make($req->txt_pass);
            $user->role=$req->slt_role;
            $user->status=$req->slt_status;
            $user->save();
            return redirect()->route('getListUser')->with(['flash_message'=>'Thêm user thành công']);
        }


    }
    public function getDelUser($id){
        $user=User::find($id);
        $user->delete();
        return redirect()->route('getListUser')->with(['flash_message'=>'Xóa user thành công']);
    }
    public function getDelAdmin($id){
        $user=Admin::find($id);
        $user->delete();
        return redirect()->route('getListAdmin')->with(['flash_message'=>'Xóa admin thành công']);
    }
    public function getListCustomer(){  
        $customer=Customer::paginate(10);
        return view('admin.customer.list',compact('customer'));
    }

    public function getListBill(){
        // $now = Carbon::now();

        $data=DB::table('bills')
                ->join('customer','customer.id','=','bills.id_customer')
                ->select('customer.name as namecus','bills.*')
                ->where('status','=',0)
                ->orderBy('id','desc')
                ->paginate(8);
        $data_payed=DB::table('bills')
            ->join('customer','customer.id','=','bills.id_customer')
            ->select('customer.name as namecus','bills.*')
            ->where('status','=',1)
            ->orderBy('id','desc')
            ->paginate(8);
            return view('admin.bill.list',compact('data','data_payed'));

    }
    public function getEditBill($id){
        $data=Bill::find($id);
        return view('admin.bill.edit',compact('data'));
}

    public function postEditBill(Request $req,$id){
            $data=Bill::find($id);
            $data->status=$req->txtstatus;
            $data->update();
            return redirect()->route('getListBill')->with(['flash_message'=>'Sửa hóa đơn thành công']);
    }

    
}
