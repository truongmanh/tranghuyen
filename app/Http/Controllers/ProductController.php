<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductEditRequest;
use App\Http\Requests\ProductRequest;
use App\Products;
use App\type_product;
use Illuminate\Http\Request;
use File;
class ProductController extends Controller
{
    public function getListProduct(){
        $data=Products::select('id','name','id_type','description','image','status')->paginate(8);
        return view('admin.product.list',compact('data'));
    }

    public function getAddProduct(){
        $data=type_product::select('id','name','parent_id')->get()->toArray();
        return view('admin.product.add',compact('data'));
    }
    public function postAddProduct(ProductRequest $req){
        $pro=new Products();
        $file=$req->file('txt_img');
        $pro->name=$req->txt_name;
        $pro->alias=str_slug($req->txt_name,'-');
        $pro->id_type=$req->slt_cate;
        $pro->description=$req->txt_content;
        $pro->unit_price=$req->txt_price;
        $pro->promotion_price=$req->txt_promo_price;
        $pro->new=$req->slt_new;
        if(strlen($file)>0){
            $filename=time().'.'.$file->getClientOriginalExtension();
            $path="uploads/product/";
            $file->move($path,$filename);
            $pro->image=$filename;
        }
        $pro->status=$req->slt_status;
        $pro->save();
        return redirect()->route('getListProduct')->with(['flash_message'=>'Thêm sản phẩm thành công']);

    }

    public function getEditProduct($id){
        $data_cate=type_product::select('id','name','parent_id')->get()->toArray();
        $data=Products::findOrFail($id)->toArray();
        return view('admin.product.edit',compact('data_cate','data'));

    }

    public function postEditProduct(ProductEditRequest $req,$id){
        $pro=Products::findOrFail($id);
        $file=$req->file('txt_img');
        $pro->name=$req->txt_name;
        $pro->alias=str_slug($req->txt_name,'-');
        $pro->id_type=$req->slt_cate;
        $pro->description=$req->txt_content;
        $pro->unit_price=$req->txt_price;
        $pro->promotion_price=$req->txt_promo_price;
        $pro->new=$req->slt_new;
        if(strlen($file)>0){
            $img_current=$req->txt_img_current;
            if(file_exists(public_path().'/uploads/product/'. $img_current)){
                File::delete(public_path().'/uploads/product/'. $img_current);
            }
            $filename=time().'.'.$file->getClientOriginalExtension();
            $path="uploads/product/";
            $file->move($path,$filename);
            $pro->image=$filename;
        }
        $pro->status=$req->slt_status;
        $pro->update();
        return redirect()->route('getListProduct')->with(['flash_message'=>'Sửa sản phẩm thành công']);
    }
    public function getDelProduct($id){
        $pro=Products::findOrFail($id);
        if(file_exists(public_path().'/uploads/product/'. $pro->image)){
            File::delete(public_path().'/uploads/product/'. $pro->image);
        }
        $pro->delete();
        return redirect()->route('getListProduct')->with(['flash_message'=>'Xóa danh mục thành công']);
    }
}
