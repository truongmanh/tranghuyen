<?php

namespace App\Http\Controllers;

use App\Http\Requests\CategoryEditRequest;
use App\Http\Requests\CategoryRequest;
use App\type_product;
use File;

class CategoryController extends Controller
{
    public function getAddCate(){
        $data=type_product::select('id','name','parent_id')->get()->toArray();
        return view("admin.category.add",compact('data'));
    }

    public function postAddCate(CategoryRequest $req){
      $cate=new type_product();
      $file=$req->file('txt_img');
      $cate->name=$req->txt_name;
      $cate->alias=str_slug($cate->name,'-');
      $cate->parent_id=$req->slt_name;
      $cate->description=$req->txt_des;
      if(strlen($file)>0){
            $filename=time().'.'.$file->getClientOriginalExtension();
            $path="uploads/cate/";
            $file->move($path,$filename);
            $cate->image=$filename;
        }
      $cate->status=$req->slt_status;
      $cate->save();
        return redirect()->route('getListCate')->with(['flash_message'=>'Thêm danh mục thành công']);
    }
    public function getEditCate($id){
        $data=type_product::findOrFail($id)->toArray();
        $data_cate=type_product::select('id','name','parent_id')->get()->toArray();
        return view('admin.category.edit',compact(['data','data_cate']));
    }

    /**
     * @param CategoryEditRequest $req
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postEditCate(CategoryEditRequest $req, $id){
        $cate=type_product::findOrFail($id);
        $file=$req->file('txt_img');
        $cate->name=$req->txt_name;
        $cate->alias=str_slug($cate->name,'-');
        $cate->parent_id=$req->slt_name;
        $cate->description=$req->txt_des;
        if(strlen($file)>0){
            $img_current=$req->txt_img_current;
            if(file_exists(public_path().'/uploads/cate/'. $img_current)){
                File::delete(public_path().'/uploads/cate/'. $img_current);
            }
            $filename=time().'.'.$file->getClientOriginalExtension();
            $path="uploads/cate/";
            $file->move($path,$filename);
            $cate->image=$filename;
        }
        $cate->status=$req->slt_status;
        if($cate->update()){
            return redirect()->route('getListCate')->with(['flash_message'=>'Sửa danh mục thành công']);
        }

    }
    public function getListCate(){
        $data=type_product::select('id','name','parent_id','status')->paginate(4);
        return view('admin.category.list',compact('data'));
    }

    public function getDelCate($id){
        $cate=type_product::findOrFail($id);
        if(file_exists(public_path().'/uploads/cate/'. $cate->image)){
                File::delete(public_path().'/uploads/cate/'. $cate->image);
        }
        $cate->delete();
        return redirect()->route('getListCate')->with(['flash_message'=>'Xóa danh mục thành công']);
    }
}
