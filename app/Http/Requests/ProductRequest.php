<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
           'txt_name'=>'required',
           'txt_content'=>'required',
            'txt_price'=>'required|integer',
            'txt_promo_price'=>'integer',
            'txt_img'=>'required|image'
        ];
    }
    public function messages()
    {
        return [
            'txt_name.required'=>'Tên không được để trống',
            // 'txt_name.unique'=>'Tên không được trùng',
            'txt_content.required'=>'Tên không được để trống',
            'txt_price.required'=>'Giá không được để trống',
            'txt_price.integer'=>'Giá không đúng định dạng',
            'txt_promo_price.integer'=>'Giam Giá không  đúng định dạng',
            'txt_img.required'=>'Ảnh không được để trống',
            'txt_img.image'=>'phải là ảnh',
        ];
    }
}
