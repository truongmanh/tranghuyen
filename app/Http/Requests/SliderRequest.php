<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SliderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'txt_img'=>'required|image',

        ];
    }
    public function messages()
    {
        return [
            'txt_img.required'=>'ảnh không được trống',
            'txt_img.image'=>'ảnh không đúng định dạng'
        ];
    }
}
