<?php

namespace App;

class WhishList
{
    public $items = null;

    public function __construct($oldwhishList){
        if($oldwhishList){
            $this->items = $oldwhishList->items;
        }
    }

    public function add($item, $id){

        $list = ['item' => $item];
        if($this->items){
            if(array_key_exists($id, $this->items)){
                $list = $this->items[$id];
            }
        }
        $this->items[$id] = $list;
    }

    public function del($id){
        unset($this->items[$id]);
    }

}
